﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bank_Limit_Tracket.DAL;

namespace Bank_Limit_Tracker.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            DatabaseHelper dh = new DatabaseHelper();
            var bankDetail = dh.GetBankDetails();

            return View(bankDetail);
        }
    }
}