﻿using Bank_Limit_Tracket.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank_Limit_Tracker.Tests
{
    [TestClass]
    public class DatabaseHelperUnitTests
    {
        [TestMethod]
        public void CompareTestDataReturnCount()
        {
            DatabaseHelper dh = new DatabaseHelper();
            var expectedCount = 5;

            var results = dh.GetBankDetails().Count;

            Assert.AreEqual(results, expectedCount);
        }
    }
}
