# BankLimitTracker


## About
This project was created as a test application for an interview to showcase various techniques and coding principals to the potential employer. At this this there is no intention of continuing on with this project nor ever put it into a "production" state. The project simply takes a set of static test data and displays it on a simple web page. The project utilizes an MVC project, as well as a data access layer (DAL) and a unit test project with a few smaller examples of tests. 

## Author
Mitchell Console - 2019