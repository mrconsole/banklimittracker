﻿using Bank_Limit_Tracket.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank_Limit_Tracket.DAL
{
    public class DatabaseHelper
    {
        public DatabaseHelper()
        {
            //initialize database connection
            //would keep connection info in Web.config and retrieve with configuration manager
        }

        public List<BankDetails> GetBankDetails()
        {
            //This is normally where I would use an ORM like Dapper to execute the stored procedure on the database and map it to a list of the return type

            var bank1 = new BankDetails() { bankName= "Bank of America", currDate=new DateTime(2019,9,27).Date, rating=7, totalAssets=1234000};
            var bank2 = new BankDetails() { bankName = "Wells Fargo", currDate = new DateTime(2019,9,27).Date, rating = -4, totalAssets = 5657345 };
            var bank3 = new BankDetails() { bankName = "Bank of Nova Scotia", currDate = new DateTime(2019,9,27).Date, rating = 2, totalAssets = 2999002 };
            var bank4 = new BankDetails() { bankName = "Royal Bank of Canada", currDate = new DateTime(2019,9,27).Date, rating = -1, totalAssets = 4346823 };
            var bank5 = new BankDetails() { bankName = "Bank of Montreal", currDate = new DateTime(2019,9,27).Date, rating = 9, totalAssets = 15342679 };

            var results = new List<BankDetails>() { bank1, bank2, bank3, bank4, bank5};

            return results;
        }
    }
}
