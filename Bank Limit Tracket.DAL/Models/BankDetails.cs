﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank_Limit_Tracket.DAL.Models
{
    public class BankDetails
    {
        public string bankName { get; set; }
        public int rating { get; set; }
        public decimal totalAssets { get; set; }

        //This would most likely be a persisted calculated column in the database but for simplicity I am going to do the calculation here (no database for this test project)
        public double calcLimit {
            get
            {
                //base limit
                double limit = 2000000;

                //adjust the limit based on the rating
                if (rating >= -5 && rating <= -3)
                {
                    limit *= 0.88;
                }
                else if (rating >= -2 && rating <= 0)
                {
                    limit *= 0.91;
                }
                else if (rating >= 1 && rating <= 3)
                {
                    limit *= 1.05;
                }
                else if (rating >= 4 && rating <= 6)
                {
                    limit *= 1.08;
                }
                else if (rating >= 7 && rating <= 10)
                {
                    limit *= 1.13;
                }
                else
                {
                    //error handing if outside of range
                }

                //increase the limit for banks with larger assets
                if (totalAssets > 3000000)
                {
                    limit *= 1.23;
                }

                return limit;
            }
        }
        public DateTime currDate { get; set; }
    }
}
